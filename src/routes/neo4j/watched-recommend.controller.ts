import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const recommendBody = z.object({
  userId: z.number(),
});

export const watchedRecommendController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const session = req.context.neo4j.session();
  try {
    const body = recommendBody.parse(req.body);

    const watchedMovies = await session.run(
      `MATCH (u:User {userId: $userId})-[:WATCHED]->(:Movie)<-[:WATCHED]-(neighbor:User)-[:WATCHED]->(recommendation:Movie)
       WHERE NOT (u)-[:WATCHED]->(recommendation)
       RETURN recommendation`,
      { userId: body.userId }
    );

    const movies = watchedMovies.records.map((record) => {
      return record.get("recommendation").properties.movieId}
    );

    

    res.json({ movies: [...new Set(movies)] });
  } catch (err: any) {
    next(err);
  }
  session.close();
};
