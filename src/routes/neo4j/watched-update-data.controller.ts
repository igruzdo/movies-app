import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const updateBody = z.object({
  userId: z.number(),
  watched: z.object({
    movieId: z.number(),
  }),
});

export const checkIfExists = async ({
  session,
  query,
  nodeName,
  params,
}: {
  session: any;
  query: string;
  nodeName: string;
  params: any;
}) => {
  const data = await session.run(
    `OPTIONAL MATCH ${query}
    RETURN ${nodeName} IS NOT NULL AS exists`,
    params
  );
  return !!data.records.find((e: any) => e._fields[0]);
};


export const watchedUpdateController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const session = req.context.neo4j.session();

  try {
    const body = updateBody.parse(req.body);
    const { watched, userId } = body;
    const { prisma } = req.context;

    const isUserExistsInNeo = await checkIfExists({
      session,
      query: `(u:User {userId: $userId})`,
      nodeName: 'u',
      params: { userId },
    });

    const isUserExistsInPg =await prisma.user.findFirst({
      where: {
        id: userId,
      }
    });

    !isUserExistsInNeo && !!isUserExistsInPg &&
      (await session.run(`CREATE (u:User {userId: $userId})`, { userId }));

    const isMovieExistsInNeo = await checkIfExists({
      session,
      query: `(d:Movie {movieId: $movieId})`,
      nodeName: 'd',
      params: { movieId: watched.movieId },
    });

    const isMovieExistsPg =await prisma.movie.findFirst({
      where: {
        id: watched.movieId,
      }
    });

    !isMovieExistsInNeo && !!isMovieExistsPg &&
      (await session.run(`CREATE (d:Movie {movieId: $movieId})`, {
        movieId: watched.movieId,
      }));

      
    await session.run(
      `MATCH (u:User {userId: $userId}), (d:Movie {movieId: $movieId})
       CREATE (u)-[:WATCHED]->(d)`,
      { userId, movieId: watched.movieId }
    );

    await prisma.watches.create({
      data: {
        user_id: userId,
        movie_id: watched.movieId
      }
    })

    res.json({
      status: "ok",
    });
  } catch (err: any) {
    next(err);
  }
  session.close();
};
