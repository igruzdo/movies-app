import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const updateBody = z.object({
  userId: z.number(),
  watched: z.object({
    movieId: z.number(),
  }),
});

export const watchesAddController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { prisma } = req.context;
  const session = req.context.neo4j.session();
  try {
    const body = updateBody.parse(req.body);
    const { userId, watched } = body;

    const isWatchExists = await prisma.watches.findFirst({
      where: {
        user_id: userId,
        movie_id: watched.movieId,
      }
    });

    !isWatchExists &&
      (await session.run(
        `MATCH (u:User {userId: $userId}), (d:Movie {movieId: $movieId})
        CREATE (u)-[:WATCHED]->(d)`,
        { userId, movieId: watched.movieId }
      )) && (
        await prisma.watches.create({
          data: {
            user_id: userId,
            movie_id: watched.movieId,
          }
        })
      );

    res.json({
      status: "ok",
    });
  } catch (err: any) {
    next(err);
  }
  session.close();
};
