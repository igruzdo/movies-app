import express from 'express';
import { storeMoviesFindByQueryController } from './elastic/store-movies-find-by-query.controller';
import { storeMoviesGenerateController } from './elastic/store-movies-generate.controller';
import { storeMoviesController } from './elastic/store-movies.controller';
import { watchesAddController } from './neo4j/watched-add.controller';
import { watchedRecommendController } from './neo4j/watched-recommend.controller';
import { watchedUpdateController } from './neo4j/watched-update-data.controller';
import { moviesAllController } from './postgre/all-movies.controller';
import { moviesOveriewController } from './postgre/movie-overview.controller';
import { moviesUserWatchesController } from './postgre/user-watches.controller';

import { errorHandler } from './utils/error.handler';
import { healthController } from './utils/health.controller';

const routes = express.Router();

routes.use('/health', healthController);

routes.post('/movies/set', storeMoviesController);
routes.post('/movies/query', storeMoviesFindByQueryController);
routes.post('/movies/overview', moviesOveriewController);
routes.post('/movies/all', moviesAllController);
routes.get('/movies/generate', storeMoviesGenerateController);

routes.post('/watches/recommend', watchedRecommendController);
routes.post('/watches/update', watchedUpdateController);
routes.post('/watches/watch', watchesAddController);
routes.post('/watches/watched', moviesUserWatchesController);

routes.use(errorHandler);

export { routes };
