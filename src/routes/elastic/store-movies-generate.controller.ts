import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const scanPagesBody = z
  .object({
    scanPages: z.number().optional(),
  })
  .optional();

export const storeMoviesGenerateController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const elasticsearch = req.context.elastic;
  const tmdb = req.context.tmdb;

  try {
    const data = scanPagesBody.parse(req.body);
    const pages = data?.scanPages || 1;
    const { prisma } = req.context;
    let maxPage = 500;

    for (let page = 1; page < maxPage && page <= pages; page += 1) {
      const moviews = await tmdb.movie.getTopRated({
        query: {
          page: page,
        },
      });

      if (!moviews.data) throw "Nothing found";

      maxPage = moviews.data.total_pages;

      for (const movie of moviews.data.results) {
        const credits = await tmdb.movie.getCredits({
          pathParameters: {
            movie_id: movie.id,
          },
        });

        const director = credits.data.crew.find(
          ({ job }) => job === "Director"
        );

        const dataToCreate = {
          title: movie.title,
          year: new Date(movie.release_date).getFullYear(),
          director: director?.name ?? '',
          overview: movie.overview,
        }

        const result = await elasticsearch.index({
          index: "movies",
          body: dataToCreate,
        });

        await prisma.movie.create({
          data: dataToCreate,
        })
      }
    }

    res.json({
      status: "ok",
    });
  } catch (err: any) {
    next(err);
  }
};
