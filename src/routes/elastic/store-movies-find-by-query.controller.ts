import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const getBody = z.object({
  query: z.string(),
});

export const storeMoviesFindByQueryController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const elasticsearch = req.context.elastic;

  try {
    const body = getBody.parse(req.body);
    const { query } = body;

    const result = await elasticsearch.search({
      index: "movies",
      body: {
        query: {
          multi_match: {
            // https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html#query-string-fuzziness
            query: query + "~5",
            fields: ["title", "director", "overview"],
          },
        },
      },
    });

    res.json({
      data: result.hits.hits.map((hit) => hit._source),
    });
  } catch (err: any) {
    next(err);
  }
};
