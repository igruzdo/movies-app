import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const storeBody = z.object({
  data: z.array(
    z.object({
      title: z.string(),
      year: z.number(),
      director: z.string(),
    })
  ),
});

export const storeMoviesController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const elasticsearch = req.context.elastic;

  try {
    const body = storeBody.parse(req.body);
    const movies = body.data;

    for (const movie of movies) {
      const result = await elasticsearch.index({
        index: "movies",
        body: movie,
      });
      console.log(result);
    }

    res.json({
      status: "ok",
    });
  } catch (err: any) {
    next(err);
  }
};
