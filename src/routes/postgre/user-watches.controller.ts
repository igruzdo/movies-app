import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const watchesBody = z.object({
  userId: z.number(),
});

export const moviesUserWatchesController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { prisma } = req.context;
  try {
    const body = watchesBody.parse(req.body);
    const { userId } = body;

    const watches = await prisma.watches.findMany({
      where: {
        user_id: userId,
      }
    });
    
    res.json({
      watches
    });
  } catch (err: any) {
    next(err);
  }
};

