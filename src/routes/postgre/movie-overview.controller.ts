import { NextFunction, Request, Response } from "express";
import { z } from "zod";

const movieBody = z.object({
  movieId: z.number(),
});

export const moviesOveriewController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { prisma } = req.context;
  try {
    const body = movieBody.parse(req.body);
    const { movieId } = body;

    const movie = await prisma.movie.findFirst({
      where: {
        id: movieId,
      }
    });
    
    res.json({
      overview: movie?.overview
    });

  } catch (err: any) {
    next(err);
  }
};

