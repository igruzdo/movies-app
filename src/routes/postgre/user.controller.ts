import { NextFunction, Request, Response } from "express";

export async function userAllController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;

    res.json(await prisma.user.findMany(req.body));
  } catch (err) {
    next(err);
  }
}

export async function userGetByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;

    res.json(await prisma.user.findFirst(req.body));
  } catch (err) {
    next(err);
  }
}

export async function userCreateController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;

    res.json(
      await prisma.user.create({
        data: req.body,
      })
    );
  } catch (err) {
    next(err);
  }
}

export async function userUpdateByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;

    res.json(await prisma.user.update(req.body));
  } catch (err) {
    next(err);
  }
}

export async function userDeleteByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;

    res.json(
      await prisma.user.delete({
        where: {
          id: parseInt(req.params.id),
        },
      })
    );
  } catch (err) {
    next(err);
  }
}
