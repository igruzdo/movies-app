import { NextFunction, Request, Response } from "express";

export const moviesAllController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { prisma } = req.context;
  try {
    const movies = await prisma.movie.findMany();
    res.json({
      movies
    });
  } catch (err: any) {
    next(err);
  }
};

