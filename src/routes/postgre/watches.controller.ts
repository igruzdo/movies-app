import { NextFunction, Request, Response } from "express";

export async function watchesGetController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;
    const data = req.body;

    res.json(await prisma.watches.findFirst({
      where: {
        user_id: data.userId,
        movie_id: data.movieId
      }
    }));
  } catch (err) {
    next(err);
  }
}

export async function watchesCreateController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;
    const data = req.body;

    res.json(await prisma.watches.create({
      data: {
        user_id: data.userId,
        movie_id: data.movieId
      }
    }));
  } catch (err) {
    next(err);
  }
}

export async function watchesDeleteByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;

    res.json(
      await prisma.watches.delete({
        where: {
          id: parseInt(req.params.id),
        },
      })
    );
  } catch (err) {
    next(err);
  }
}

