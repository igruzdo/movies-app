import { NextFunction, Request, Response } from "express";

export async function moviesGetByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;

    res.json(await prisma.movie.findFirst(req.body));
  } catch (err) {
    next(err);
  }
}

export async function moviesCreateController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;

    res.json(
      await prisma.movie.create({
        data: req.body,
      })
    );
  } catch (err) {
    next(err);
  }
}


export async function moviesDeleteByIdController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const { prisma } = req.context;

    res.json(
      await prisma.movie.delete({
        where: {
          id: parseInt(req.params.id),
        },
      })
    );
  } catch (err) {
    next(err);
  }
}

