import { driverNeo4j, elasticsearch, tmdb, prisma } from "../db";

export {};

declare global {
  namespace Express {
    export interface Request {
      context: {
        elastic: typeof elasticsearch;
        tmdb: typeof tmdb;
        neo4j: typeof driverNeo4j
        prisma: typeof prisma
      };
    }
  }
}
