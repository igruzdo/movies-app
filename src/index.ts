import morgan from "morgan";
import express from "express";
import { NextFunction, Request, Response } from "express";
import axios from "axios";
import fs from 'fs';
import { extendContextWithDb } from "./db";
import * as dotenv from "dotenv"; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import { routes } from "./routes";
import bodyParser from "body-parser";

dotenv.config();

const app = express();
const port = process.env.PORT;

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(extendContextWithDb);
app.use(routes);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
