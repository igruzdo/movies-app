import neo4j from 'neo4j-driver';

const driverNeo4j = neo4j.driver(
    `bolt://localhost:${process.env.NEO4J_PORT ?? 7687}`,
    neo4j.auth.basic('neo4j', process.env.NEO4J_PASSWORD ?? 'password')
);

export {
  driverNeo4j
}
