import { NextFunction, Request, Response } from "express";
import { driverNeo4j } from "./neo4j";
import { elasticsearch } from "./elasticsearch";
import { tmdb } from "./tmdb";
import { prisma } from "./prisma";

const extendContextWithDb = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  req.context = req.context || {};
  req.context.elastic = elasticsearch;
  req.context.tmdb = tmdb;
  req.context.prisma = prisma;
  req.context.neo4j = driverNeo4j;
  next();
};

export {
  extendContextWithDb,
  elasticsearch,
  tmdb,
  prisma,
  driverNeo4j
}
